/**
 * TP1 dans le cadre du cours INF3135 gr. 20
 * Fait par Lou-Gomes Neto (NETL14039105)
 * Automne 2017, UQAM
 * 
 * PANDEMIC
 * 
 * Ce programme prend en entrée une carte et demontre la progression
 * d'un virus a travers le nombre de jours specifié en argument.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

//----------
//CONSTANTES
//----------

//DIMENSIONS DE LA CARTE
#define LIGNES      20
#define COLONNES    40
#define TOTAL_CARTE 800

//INTERSECTION DE LA CARTE
#define INTER(x,y)  (COLONNES * x + y)

//LIMITES DES JOURS A AFFICHER
#define JOURS_MIN   0
#define JOURS_MAX   100

//CELLULES POSSIBLES
#define SAINE           'H'
#define VIRUS_X         'X'
#define SANS_POPULATION '.'

//VOISINS SELON LES LIMITES
const int VOISINS_COIN_NO[] =   {5,7,8}; //Nord-Ouest
const int VOISINS_COIN_NE[] =   {4,6,7}; //Nord-Est
const int VOISINS_COIN_SO[] =   {2,3,5}; //Sud-Ouest
const int VOISINS_COIN_SE[] =   {1,2,4}; //Sud-Est
const int VOISINS_LIGNE_MIN[] = {4,5,6,7,8}; //Nord
const int VOISINS_LIGNE_MAX[] = {1,2,3,4,5}; //Sud
const int VOISINS_COL_MIN[] =   {2,3,5,7,8}; //Ouest
const int VOISINS_COL_MAX[] =   {1,2,4,6,7}; //Est
const int VOISINS_MAX[] =       {1,2,3,4,5,6,7,8}; //Interieur

//NOMBRE DE VOISINS POSSIBLES
#define NB_VOISINS_COINS  3
#define NB_VOISINS_BORDS  5
#define NB_MAX_VOISINS    8

//------------
//DECLARATIONS
//------------

void validerNbArguments(int nbArgs);
long obtenirJours(int argc, char const *argv[]);
void validerJours(long jours);
void remplirCarte(char *carte);
void celluleInvalide(char cellule, int eof);
bool estPasCelluleValide(char cellule);
void afficherCarte(char *carte, long jours);
void modifierCarte(char *carte);
char positionVoisins(char *carte, int i, int j);
char trouverVoisins (char *carte, const int *voisinsPossibles, int i, int j, int nbVoisins);
char propagation(char cellule, char *totalVoisins);

//-----------
//DEFINITIONS
//-----------

/**
 * Valide le nombre d'arguments passes au main, affiche un message d'erreur
 * s'il y a trop d'arguments et quite le programme si la validation echoue.
 * 
 * @param nbArgs nombre d'arguments passes au programme
 */
void validerNbArguments(int nbArgs) {

  if (nbArgs > 2) {
    printf("Erreur: Attendu un seul argument: le nombre de jours a simuler\n");
    exit(1);
  }
}

/**
 * Obtiens le nombre de jours en string et le converti en variable de type long. 
 * Retourne ensuite le nombre de jours.
 * 
 * @param argc le nombre d'arguments du main
 * @param argv[] les arguments du main
 * @return jours le nombre de jours pour la progression du virus
 */
long obtenirJours(int argc, char const *argv[]) {

  char *strJours;
  char *end;
  long jours;

  if(argc == 2) {
    strJours = argv[1];
    jours = strtol(strJours, &end, 10);
  }else{
    jours = 0;
  }

  validerJours(jours);

  return jours;
}

/**
 * Affiche un message d'erreur si le jours en parametre n'est pas entre 0 et 100. 
 * Quite le programme si la validation echoue.
 * 
 * @param jours le nombre de jours a valider
 */
void validerJours(long jours) {
  if(jours < JOURS_MIN || jours > JOURS_MAX) {
    printf("Erreur: Le nombre de jours à simuler doit être entre 0 et 100.");
    exit(1);
  }
}

/**
 * Remplis la carte de configuration par l'entree standard. 
 * Appel une fonction d'erreur s'il y a un caractere invalide ou si la carte 
 * n'est pas remplis.
 * 
 * @param carte pointeur vers la carte de configuration a remplir   
 */
void remplirCarte(char *carte) {

  int c;
  int i = 0;
  int eof = 0;

  while(i < TOTAL_CARTE && (c = getchar()) != EOF) { 
    if(c == '\n' || c == ' ' || c == '\t') continue;
    if(estPasCelluleValide(c)) celluleInvalide(c, eof);
    carte[i++] = c;
  }

  if(i < TOTAL_CARTE) { //Carte n'est pas remplie
    eof = -1;
    celluleInvalide(c, eof);
  } 
}

/**
 * Retourne vrai si la cellule contient un caractere invalide.
 * 
 * @param cellule a valider
 * @return cellule invalide ou valide
 */
bool estPasCelluleValide(char cellule) {
  return cellule != SANS_POPULATION && cellule != SAINE && cellule != VIRUS_X;
}

/**
 * Affiche un message d'erreur contenant la cellule invalide et 
 * quitte le programme.
 * 
 * @param cellule la cellule invalide
 * @param eof -1 si la fin du fichier est arrivee trop tot
 */
void celluleInvalide(char cellule, int eof) {

  if(eof == 0) {
    printf("Erreur: Caractère `%c` inattendu, attendu `H`, `X` ou `.`.\n", cellule);
  }else{
    printf("Erreur: Caractère `EOF` inattendu, attendu `H`, `X` ou `.`.\n");
  }

  exit(1);
}

/**
 * Affiche la carte et son evolution selon le nombre de jours fournis en parametre.
 * 
 * @param carte pointeur vers la carte de configuration
 * @param jours le nombre de jours a afficher la carte
 */
void afficherCarte(char *carte, long jours) {

  long progression = 0;

  while(progression <= jours) {
    printf("Jour %ld\n", progression); 
    progression++;

    int i;
    int j;
    for(i = 0; i < LIGNES; i++) {
      for(j = 0; j < COLONNES; j++) {
        putchar(carte[INTER(i,j)]);
      }
      printf("\n");
    }
    modifierCarte(carte);
  }
}

/**
 * Modifie la carte de configuration pour passer au jour suivant
 * 
 * @param carte pointeur vers la carte de configuration
 */
void modifierCarte(char *carte) {

  char jourSuivant[TOTAL_CARTE];

  int i;
  int j;
  for(i = 0; i < LIGNES; i++) {
    for(j = 0; j < COLONNES; j++) { 
      jourSuivant[INTER(i,j)] = positionVoisins(carte, i, j);
    }
  }

  for(i = 0; i < LIGNES; i++) {
    for(j = 0; j < COLONNES; j++) {
      carte[INTER(i,j)] = jourSuivant[INTER(i,j)];
    }
  }
}

/**
 * Verifie la position de la cellule a l'intersection (i,j) de la carte 
 * et determine les voisins possibles de cette cellule. 
 * Retourne l'etat de la cellule selon l'etat des cellules voisines
 * 
 * @param carte de configuration
 * @param i premier point de coordonnee de la cellule
 * @param j deuxieme point de coordonnee de la cellule
 * @return etatCellule le nouvel etat de la cellule
 */
char positionVoisins(char *carte, int i, int j) {
  
  char etatCellule;

  //Coin, donc 3 voisins possibles
  if(i == 0 && j == 0) { //Coin nord-ouest
    etatCellule = trouverVoisins(carte, VOISINS_COIN_NO, i, j, NB_VOISINS_COINS);
  }else if(i == 0 && j == COLONNES-1) { //Coin nord-est
    etatCellule = trouverVoisins(carte, VOISINS_COIN_NE, i, j, NB_VOISINS_COINS);
  }else if(i == LIGNES-1 && j == 0) { //Coin sud-ouest
    etatCellule = trouverVoisins(carte, VOISINS_COIN_SO, i, j, NB_VOISINS_COINS);
  }else if(i == LIGNES-1 && j == COLONNES-1) { //Coin sud-est
    etatCellule = trouverVoisins(carte, VOISINS_COIN_SE, i, j, NB_VOISINS_COINS);
  }else{ //Bordures, donc 5 voisins possibles
    if(i == 0) { //Limite nord (ligne 0)
      etatCellule = trouverVoisins(carte, VOISINS_LIGNE_MIN, i, j, NB_VOISINS_BORDS);
    }else if(i == LIGNES-1) { //Limite sud (ligne 19)
      etatCellule = trouverVoisins(carte, VOISINS_LIGNE_MAX, i, j, NB_VOISINS_BORDS);
    }else if(j == 0) { //Limite ouest (colonne 0)
      etatCellule = trouverVoisins(carte, VOISINS_COL_MIN, i, j, NB_VOISINS_BORDS);
    }else if(j == COLONNES-1) { //Limite est (colonne 39)
      etatCellule = trouverVoisins(carte, VOISINS_COL_MAX, i, j, NB_VOISINS_BORDS);
    }else{ //Interieur de la carte, donc 8 voisins possibles
      etatCellule = trouverVoisins(carte, VOISINS_MAX, i, j, NB_MAX_VOISINS);
    }
  }

  return etatCellule;
}

/**
 * Trouve les voisins de la cellule a l'intersection (i,j) de la carte et
 * retourne l'etat de la cellule selon ses voisins.
 * 
 * @param carte de configuration
 * @param voisinsPossibles voisins de la cellule qui ont un indexe valide
 * @param i premier point de coordonnee de la cellule
 * @param j deuxieme point de coordonnee de la cellule
 * @param nbVoisins total des voisins possibles (length de voisinsPossibles)
 * @return char le nouvel etat de la cellule
 */
char trouverVoisins (char *carte, const int *voisinsPossibles, int i, int j, int nbVoisins) {

  char cellule = carte[(INTER(i,j))];
  char totalVoisins[8] = 
  {
    SANS_POPULATION, SANS_POPULATION, SANS_POPULATION, SANS_POPULATION, 
    SANS_POPULATION, SANS_POPULATION, SANS_POPULATION, SANS_POPULATION
  };

  int voisin;
  for(voisin = 0; voisin < nbVoisins; voisin++) {
    switch(voisinsPossibles[voisin]) {
      case 1 : totalVoisins[0] = carte[(INTER(i,j)) - (COLONNES+1)]; break; //Nord-ouest
      case 2 : totalVoisins[1] = carte[(INTER(i,j)) - COLONNES]; break; //Nord
      case 3 : totalVoisins[2] = carte[(INTER(i,j)) - (COLONNES-1)]; break; //Nord-est
      case 4 : totalVoisins[3] = carte[(INTER(i,j)) - 1];  break; //Ouest
      case 5 : totalVoisins[4] = carte[(INTER(i,j)) + 1];  break; //Est
      case 6 : totalVoisins[5] = carte[(INTER(i,j)) + (COLONNES-1)]; break; //Sud-ouest
      case 7 : totalVoisins[6] = carte[(INTER(i,j)) + COLONNES]; break; //Sud
      case 8 : totalVoisins[7] = carte[(INTER(i,j)) + (COLONNES+1)]; break; //Sud-Est
    }
  } 

  return propagation(cellule, totalVoisins);
} 

/**
 * Affecte la cellule en parametre selon l'etat de ses voisins.
 * Retourne la cellule modifie.
 * 
 * @param la cellule a modifier
 * @param totalVoisins les voisins de la cellule
 * @return cellule le nouvel etat de la cellule
 */
char propagation(char cellule, char *totalVoisins) {

  int voisinsInfectes = 0;
  int voisinsSains = 0;
  int voisinsVivants;

  int i;
  for(i = 0; i < NB_MAX_VOISINS; i++) { 
    if(totalVoisins[i] == VIRUS_X) {
      voisinsInfectes++;
    }else if(totalVoisins[i] == SAINE) {
      voisinsSains++;
    }
  }

  voisinsVivants = voisinsInfectes + voisinsSains;

  if(voisinsVivants < 2 || voisinsVivants > 3){ //mort solitude || mort etouffement
    cellule = SANS_POPULATION;
  }else{
    if(voisinsVivants == 3) //si assé de voisins pour populer
      cellule = SAINE;
    if(voisinsInfectes > voisinsSains && cellule == SAINE) //si majorité infecté
      cellule = VIRUS_X;
  }

  return cellule;
}

//------------------------------------//
//----------------MAIN----------------//
//------------------------------------//

/**
 * Prend un nombre de jours en argument pour la progression du virus, lis une
 * carte par l'entree standard et la modifie de jours en jours.
 * 
 * @param argc le nombre d'arguments au programme
 * @param argv les arguments du programme
 * @return code de sortie
 */
int main(int argc, char const *argv[]) {

  long jours;
  char carte[TOTAL_CARTE];

  //Verifier s'il y a trop d'arguments au main.
  validerNbArguments(argc);
  
  //Recuperation du nombre de jours.
  jours = obtenirJours(argc, argv);
  
  //Remplissage de la matrice via l'entrée standard.
  remplirCarte(carte);
  
  //affichage de la progression du virus pendant un certain nombre de 'jours' 
  afficherCarte(carte, jours);

  return 0;
}
